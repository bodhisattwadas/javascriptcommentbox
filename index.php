<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>
            <?php if(isset($_GET['forgot-password']))  : ?>
            Retrieve password !!   
            <?php elseif(isset($_GET['register']))  : ?>
            Register yourself !!
            <?php else  : ?>
            Login!!
            <?php endif; ?>
        </title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- <link href="https://fonts.googleapis.com/css?family=Quicksand:300" rel="stylesheet"> -->
        <script src="http://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript">
        function _onClickLoginRegistrationFPass(type) {
        var postData = type+"&"+$("#"+type).serialize();
        $.ajax(
            {
                url : "phpDirectory/formSubmit.php",
                type : "POST",
                dataType : "json",
                data : postData,
                success:function(data)
                {
                   if(type != 'login-form')_generateMessageGeneric(data,'mDiv','index.php');
                   else _generateMessageGeneric(data,'mDiv','landing.php');
                }
            });
    }
    function _generateMessageGeneric(data,container,reDirectURL){
        if(data[0] == '0') {
            var msg = '<div class="alert alert-danger">'+
                '<strong>Warning!!! </strong>'+
                data[1]+
                '</div>';
            $('#'+container).empty();
            $('#'+container).append(msg);
        }
        else{
            var msg = '<div class="alert alert-success">'+
                '<strong>Information!!! </strong>'+
                data[1]+
                '</div>';
            $('#'+container).empty();
            $('#'+container).append(msg);
            if(reDirectURL == 'none'){
                //window.location.reload(true);
            }else{
                window.location.href = reDirectURL; 
            }
        }
    };
        </script>
    </head>
    <body style="padding: 10px;">
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                    <h4>
                    <?php if(isset($_GET['forgot-password']))  : ?>
                    Forgot your password ?    
                    <?php elseif(isset($_GET['register']))  : ?>
                    Register yourself !!
                    <?php else  : ?>
                    Enter login credentials !!
                    <?php endif; ?>
                    </h4>    
                    
                    </div>
                    <div class="panel-body">
                    <div id="mDiv"></div>
                    <?php if(isset($_GET['forgot-password']))  : ?>
                    <form id="forgot-password-form">
                    <div class="form-group"><input type="email" class="form-control" placeholder="Enter registered email address." name="param[]"></div>
                    <button type="button" class="btn btn-danger" onclick="_onClickLoginRegistrationFPass('forgot-password-form')"><i class="fa fa-key"></i> Submit</button>
                    </form>
                    <br>    
                    <a href="index.php">Suddenly remembered Password ?</a>
                    <br>
                    <a href="?register">Want to register yourself ?</a>
                     
                    <?php elseif(isset($_GET['register']))  : ?>
                    <form id="register-form">
                    <div class="form-group"><input type="text" class="form-control" placeholder="Enter name of the user." name="param[]"></div>
                    <div class="form-group"><input type="text" class="form-control" placeholder="Choose a uniques username." name="param[]"></div>
                    <div class="form-group"><input type="email" class="form-control" placeholder="Enter email address." name="param[]"></div>
                    <div class="form-group"><input type="password" class="form-control" placeholder="Enter a strong password." name="param[]"></div>
                    <div class="form-group"><input type="password" class="form-control" placeholder="Retype the password." name="param[]"></div>
                    <button type="button" class="btn btn-success" onclick="_onClickLoginRegistrationFPass('register-form')"><i class="fa fa-sign-in"></i> Register</button>
                    </form>
                    <br>    
                    <a href="index.php">Already registered ?</a>
                    <br>
                    <a href="?forgot-password">Forgot password ?</a>
                    <?php else  : ?>
                    <form id="login-form">
                    <div class="form-group"><input type="text" class="form-control" placeholder="Enter username." name="param[]"></div>
                    <div class="form-group"><input type="password" class="form-control" placeholder="Enter password." name="param[]"></div>
                    <button type="button" class="btn btn-success" onclick="_onClickLoginRegistrationFPass('login-form')"><i class="fa fa-sign-in"></i> Login</button>
                    </form>
                    <br>    
                    <a href="?register">Want to register yourself ?</a>
                    <br>
                    <a href="?forgot-password">Forgot password ?</a>
                    <?php endif; ?>    
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </body>
</html>
