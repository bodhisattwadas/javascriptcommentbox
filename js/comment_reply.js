function _checkForValidURLReply(URL,_userId,_commentID){
    $.post( "phpDirectory/checkValidURL.php" ,
			{
				URL : URL
			}			
			)
			.fail(
				function()   		 
				{	
                                    console.log( "ERROR______FROM THE SERVER!!!!!!!!!!!:" );
				}
                        )
			.done(
				function( data )   
				{	
                                    var valFromServer = jQuery.parseJSON(data);
                                    if(valFromServer[0] == 'error'){
                                        console.log('No modal');
                                        $.post( "phpDirectory/comment_reply.php" ,
                                        {
                                            task : "comment_reply",      
                                            Userid : _userId,
                                            Reply : URL,
                                            CommentID : _commentID
                                        })
                                        .fail(
                                            function()   		 
                                            {	
                                                console.log( "ERROR______FROM THE SERVER!!!!!!!!!!!:" );
                                            }
                                        )
                                        .done(
                                            function( data )   
                                            {	
                                                comment_reply_insert( jQuery.parseJSON( data ) );
                                                console.log( "RESPONSE TEXT FROM THE SERVER!!!! data: !!!!!!!:" + data );
                                            }
                                        );
                                        $( '#text_'+_commentID ).val("");
                                    }else{
                                        _openReplyModal(valFromServer,_commentID,_userId);
                                    }
				}
			); 
}
function comment_reply_post_btn_click(_commentID)
 {
        var _reply = $('#text_'+_commentID).val();    //Here
	var _userId = $('#userID').val(); 
        if(_reply.length > 0 && _userId != null){ 
                $('#text_'+_commentID).removeAttr("style");  ///black
                _checkForValidURLReply(_reply,_userId,_commentID);
        }else{
                $('#text_'+_commentID).css( 'border' , '1px solid #ff0000' );  ///RED
                console.log( "The text area was empty." );  
        }
	}
function comment_reply_insert_modal( data ){
        var t = '<li class="comment-reply-container" id = "'+'holder_'+data.comment.id+'">';
            t += '<div class="row">';
            t += '<div class="col-md-1"><div class="user-img"><img src="'+data.user.profile_img+'" class="user-img-pic"/></div></div>';
            t += '<div class="col-md-11"><div class="comment-body">';
            t += '<h3 class="username-field">'+data.user.userName+'</h3>';
            t += '<div class="comment-text"><h4>'+data.comment.replyTitle+'</h4>';
            t += '<div class="row">';
            t += '<div class="col-md-2"><image src="'+data.comment.replyImage+'" style="width:100%;" ></div>';
            t += '<div class="col-md-10">'+data.comment.replyDescription+'</div>';
            t += '</div>';        
            t += '</div>';
            t += '</div>';
            t += '</div>';
            t += '</div>';
            t += '<button class="btn btn-danger btn-xs delete-reply-button" id="'+data.comment.id+'">Delete</button>';
            t += '</li>';
	$( '#commentHolder_'+data.comment.comment ).prepend( t );
        add_delete_reply_handlers();
    }
function comment_reply_insert( data )
	{
            
        var t = '<li class="comment-reply-container" id = "'+'holder_'+data.comment.id+'">';
            t += '<div class="row"><div class="col-md-1"><div class="user-img"><img src="'+data.user.profile_img+'" class="user-img-pic"/></div></div>';
            t += '<div class="col-md-11">';
            t += '<h3 class="username-field">'+data.user.userName+'</h3>';
            t += '<div class="comment-text">'+data.comment.reply+'</div>';
            t += '</div></div>';
            t += '<button class="btn btn-danger btn-xs delete-reply-button" id="'+data.comment.id+'">Delete</button>';
            t += '</li>';
	$( '#commentHolder_'+data.comment.comment ).prepend( t );
        add_delete_reply_handlers();
    }	
function _openReplyModal(val,_commentID,_userId){
    var modalHeaderReply = document.getElementById('modalHeaderReply');
    var modalBodyReply = document.getElementById('modalBodyReply');
    var modalImageReply = document.getElementById('modalImageReply');
    var modalCommentIDHolder = document.getElementById('commentID');
    modalHeaderReply.innerHTML = val[0];
    modalBodyReply.innerHTML = val[1];
    modalImageReply.src = val[2];
    modalCommentIDHolder.innerHTML = _commentID;
    $("#myReplyModal").modal();
}
function replyModalOnclick(){
    var modalHeaderReplyValue = document.getElementById('modalHeaderReply').innerHTML;
    var modalBodyReplyValue = document.getElementById('modalBodyReply').innerHTML;
    var modalImageReplyValue = document.getElementById('modalImageReply').src;
    var modalCommentIDHolderValue = document.getElementById('commentID').innerHTML;
    console.log(modalBodyReplyValue);
    console.log(modalHeaderReplyValue);
    console.log(modalImageReplyValue);
    console.log(modalCommentIDHolderValue);
    $.post( "phpDirectory/comment_reply.php" ,
            {
                task : "modal_comment_reply",      
                Userid : $('#userID').val(),
                CommentID : modalCommentIDHolderValue,
                modalTitle : modalHeaderReplyValue,
                modalDescription : modalBodyReplyValue,
                modalImage : modalImageReplyValue
            })
            .done(
                function( data )   
                {	
                    //comment_reply_insert( jQuery.parseJSON( data ) );
                    comment_reply_insert_modal(jQuery.parseJSON( data ));
                    document.getElementById('myReplyModal').style.display = "none";
                    console.log( "RESPONSE TEXT FROM THE SERVER!!!! data: !!!!!!!:" + data );
                }
            );
        $( '#text_'+modalCommentIDHolderValue).val("");
        $('#myReplyModal').modal('hide');
}

	