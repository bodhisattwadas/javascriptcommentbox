$( document ).ready( function(){
      	add_delete_handlers();	
        add_delete_reply_handlers();
});
function comment_delete(_comment_id){
    $.post('phpDirectory/comment_delete.php',
             {
                 task : "comment_delete",
                 comment_id : _comment_id
             }
        )
        .done(function(data){
                //console.log("success");
                $('#'+_comment_id).detach();
         });
}
function reply_delete(_reply_id){
    $.post('phpDirectory/comment_delete.php',
             {
                 task : "reply_delete",
                 reply_id : _reply_id
             }
        )
        .done(function(data){
                console.log("success");
                $('#holder_'+_reply_id).detach();
         });
}
function add_delete_handlers(){
    $('.delete-btn').each(function(){
          var btn = this;
          $(btn).click(function(){
             console.log("The button id is : "+btn.id);
             comment_delete(btn.id);
          });
      }); 
}
function add_delete_reply_handlers(){
    $('.delete-reply-button').each(function(){
          var btn = this;
          $(btn).click(function(){
             console.log("The button id is : "+btn.id);
             reply_delete(btn.id);
          });
      }); 
}