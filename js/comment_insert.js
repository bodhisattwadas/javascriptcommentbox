
$( document ).ready( function(){
        $( '#comment-post-text' ).val("");
	$( '#comment-post-btn' ).click( function( ){     
			comment_post_btn_click();
	});
        $( '#modal-post-button' ).click( function( ){     
                        _modalPostOnClick();
	});
});
function _modalPostOnClick(){
    //console.log('hello world');
    var modalHeader = document.getElementById('modalHeader');
    var modalBody = document.getElementById('modalBody');
    var modalImage = document.getElementById('modalImage');
    var _userId = $( '#userID' ).val();  
    console.log(modalHeader.innerHTML+' '+modalBody.innerHTML+' '+modalImage.src);
    $.post( "phpDirectory/comment_insert.php" ,
                                            {
                                                            task : "modal_comment_insert",      
                                                            Userid : _userId,
                                                            modalHeader : modalHeader.innerHTML,
                                                            modalBody : modalBody.innerHTML,
                                                            modalImage : modalImage.src

                                            }
                                            )
                                            .fail(
                                                    function()   		 
                                                    {	
                                                        console.log( "ERROR______FROM THE SERVER!!!!!!!!!!!:" );
                                                    }
                                            )
                                            .done(
                                                    function( data )   
                                                    {	
                                                            modal_comment_insert(jQuery.parseJSON(data));
                                                            console.log( "RESPONSE TEXT FROM THE SERVER!!!! data: !!!!!!!:" + data);
                                                            var modal = document.getElementById('myModal');
                                                            modal.style.display = "none";
                                                    }
                                            );
                                            $( '#comment-post-text' ).val("");
                                            $('#commentPostModal').modal('hide');
    
}

function _checkForValidURL(URL,_userId,_userName){
    $.post( "phpDirectory/checkValidURL.php" ,
			{
				URL : URL
			}			
			)
			.fail(
				function()   		 
				{	
                                    console.log( "ERROR______FROM THE SERVER!!!!!!!!!!!:" );
				}
                        )
			.done(
				function( data )   
				{
                                    //console.log(val);
                                    val = jQuery.parseJSON(data);
                                    if(val[0] == 'error'){
                                        console.log('No modal');
                                        $.post( "phpDirectory/comment_insert.php" ,
                                        {
                                            task : "comment_insert",      
                                            Userid : _userId,
                                            Comment : URL

                                        }
                                        )
                                        .fail(
                                            function()   		 
                                            {	
                                                console.log( "ERROR______FROM THE SERVER!!!!!!!!!!!:" );
                                            }
                                        )
                                        .done(
                                            function( data )   
                                            {	
                                                console.log(data);
                                                comment_insert( jQuery.parseJSON( data ) );
                                                console.log( "RESPONSE TEXT FROM THE SERVER!!!! data: !!!!!!!:" + data );
                                            }
                                        );
                                        $( '#comment-post-text' ).val("");
                                    }else{
                                        _openModal(val);
                                    }
				}
			); 
}
		
function comment_post_btn_click()
 {
                var _comment = $( '#comment-post-text' ).val(); 
                var _userId = $( '#userID' ).val();       
		var _userName = $( '#userName' ).val();
                if( _comment.length > 0 && _userId != null) {
                    $('#comment-post-text').removeAttr("style");  ///black
                    _checkForValidURL(_comment,_userId,_userName);
                }else{
                    $('#comment-post-text').css( 'border' , '1px solid #ff0000' );  ///RED
                    console.log( "The text area was empty.qqq" ); 
                }  
	}
	
function comment_insert( data ){
	var t = '';        
            t += '<li class="comment-holder" id="'+data.comment.comment_id+'">';
            t += '<div class="row">';
            t += '<div class="col-md-1"><div class="user-img"><img src="'+data.user.profile_img+'" class="user-img-pic"  /></div></div>';
            t += '<div class="col-md-11">';
            t += '<h3 class="username-field">'+data.user.userName+'</h3>';
            t += '<div class="comment-text">'+data.comment.comment[0]+'</div>';
            if(data.comment.comment[1] !== undefined){t+=  '<div class="comment-text"><img src="uploads/'+data.comment.comment[1]+'" class="img-thumbnail img-responsive" alt="Cinque Terre"></div>';}
            t += '</div>';
            t += '</div>';
            t += '<div class="comment-buttons-holder">';
            t += '<ul>';
            t += '<li class="delete-btn" id="'+data.comment.comment_id+'">X</li>';
            t += '</ul>';					
            t += '</div><div class="container-fluid" style="padding:5px;" id="class_'+data.comment.comment_id+'"><div class="row">';
            t += '<div class="col-md-11"><div class="form-group"><textarea class="form-control" rows="1" id="text_'+data.comment.comment_id+'"></textarea></div></div>';
            t += '<div class="col-md-1"><button type="button" class="btn btn-primary btn-sm pull-right" onclick="comment_reply_post_btn_click(\''+data.comment.comment_id+'\')">Reply</button></div>';
            t += '</div></div><ul id="commentHolder_'+data.comment.comment_id+'"></ul>';
            t += '</li>';
	$( '.comments-holder-ul' ).prepend( t );
        add_delete_handlers();
	}
function modal_comment_insert( data ){
	var t = '';        
            t += '<li class="comment-holder" id="'+data.comment.comment_id+'">';
            t += '<div class="row">';
            t += '<div class="col-md-1"><div class="user-img"><img src="'+data.user.profile_img+'" class="user-img-pic"  /></div></div>';
            t += '<div class="col-md-11">';
            t += '<h3 class="username-field">'+data.user.userName+'</h3>';
            t += '<div class="comment-text"><h3>'+data.comment.commentTitle+'</h3>';
            t += '<div class="row">';
            t += '<div class="col-md-2"><image src="'+data.comment.commentImage+'" style="width:100%;" ></div>';
            t += '<div class="col-md-10">'+data.comment.commentDescription+'</div></div>';
            t += '</div>';
            t += '</div>';
            t += '</div>';
            t += '</div>';
            t += '<div class="comment-buttons-holder">';
            t += '<ul>';
            t += '<li class="delete-btn" id="'+data.comment.comment_id+'">X</li>';
            t += '</ul>';					
            t += '</div><div class="container-fluid" style="padding:5px;" id="class_'+data.comment.comment_id+'"><div class="row">';
            t += '<div class="col-md-11"><div class="form-group"><textarea class="form-control" rows="1" id="text_'+data.comment.comment_id+'"></textarea></div></div>';
            t += '<div class="col-md-1"><button type="button" class="btn btn-primary btn-sm pull-right" onclick="comment_reply_post_btn_click(\''+data.comment.comment_id+'\')">Reply</button></div>';
            t += '</div></div><ul id="commentHolder_'+data.comment.comment_id+'"></ul>';
            t += '</li>';
	$( '.comments-holder-ul' ).prepend( t );
        add_delete_handlers();
	} 

function _openModal(val){
    var modalHeader = document.getElementById('modalHeader');
    var modalBody = document.getElementById('modalBody');
    var modalImage = document.getElementById('modalImage');
    modalHeader.innerHTML = val[0];
    modalBody.innerHTML = val[1];
    modalImage.src = val[2];
    $("#commentPostModal").modal();
}	