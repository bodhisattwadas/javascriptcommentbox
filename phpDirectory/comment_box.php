<?php require_once 'db_connect.php'; ?>
<?php if(isset($GLOBALS['comments']) && is_array($comments)): ?>
<?php foreach( $comments as $key => $comment ): ?>
<?php $user = Subscribers::getSubscriber($comment->userId); ?>
<li class="comment-holder" id="<?php echo $comment->comment_id; ?>">
<div class="row">
    <div class="col-md-1"><div class="user-img"><img src="<?php echo $user->profile_img; ?>" class="user-img-pic"/></div></div>
    <div class="col-md-11">
        <h3 class="username-field"><?php echo $user->userName; ?></h3>
        <?php $tmp = $comment->comment; $finalArray = unserialize($tmp); $size = sizeof($finalArray); ?>
        <?php if($size == 1) :?>
        <div class="comment-text"><?php echo $finalArray[0]; ?></div>
        <?php elseif($size == 2) :?>
        <div class="comment-text"><?php echo $finalArray[0]; ?></div>
        <div class="comment-text"><img src="uploads/<?php echo $finalArray[1]; ?>" class="img-thumbnail img-responsive" alt="Cinque Terre"></div>
        <?php else :?>
        <div class="comment-text">
            <h3><?php echo $finalArray['title'];?></h3>
            <div class="row">
                <div class="col-md-2"><image src="<?php echo $finalArray['image'];?>" style="width:100%;"/></div>
                <div class="col-md-10"><?php echo $finalArray['body'];?></div>
            </div>
        </div>
        <?php endif; ?>
    </div>
</div>
<?php if($userID == $comment->userId): ?>
    <div class="comment-buttons-holder"><ul><li class="delete-btn" id="<?php echo $comment->comment_id; ?>">X</li></ul></div>
<?php endif; ?>    


<div class="container-fluid" style="padding: 5px;" id="<?php echo 'class_'.$comment->comment_id; ?>">
    <div class="row">
        <div class="col-md-10"><div class="form-group"><textarea class="form-control" rows="1" id="<?php echo 'text_'.$comment->comment_id; ?>"></textarea></div></div>
        <div class="col-md-2">
            <div class="btn-group pull-right">
                <!-- <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-camera" aria-hidden="true"></i></button>-->
                <button type="button" class="btn btn-primary btn-sm pull-right" onclick="comment_reply_post_btn_click('<?php echo $comment->comment_id; ?>')">Reply</button>
            </div>
        </div>
    </div>
</div>
<?php $replies = Comments::getReplies($comment->comment_id); ?>
<ul id="<?php echo 'commentHolder_'.$comment->comment_id; ?>">
 <?php foreach( $replies as $key => $reply ): ?>
    <?php $userReply = Subscribers::getSubscriber($reply->userID); ?>
        <li class="comment-reply-container" id="<?php echo 'holder_'.$reply->replyID; ?>">
            <div class="row">
                <div class="col-md-1"><div class="user-img"><img src="<?php echo $userReply->profile_img; ?>" class="user-img-pic"/></div></div>
                <div class="col-md-11">
                    <h3 class="username-field"><?php echo $userReply->userName; ?></h3>
                    <?php $tmp = $reply->replyText; $finalArray = unserialize($tmp); $size = sizeof($finalArray); ?>
                    <?php if($size == 1) :?>
                      <div class="comment-text"><?php echo $finalArray[0]; ?></div>
                    <?php else :?>
                    <div class="comment-text">
                          <h4><?php echo $finalArray['title'];?></h4>
                          <div class="row">
                              <div class="col-md-2"><image src="<?php echo $finalArray['image'];?>" style="width:100%;"/></div>
                              <div class="col-md-10"><?php echo $finalArray['body'];?></div>
                          </div>
                      </div>
                    <?php endif; ?> 
                </div> 
            </div>
            <?php if($userID == $reply->userID): ?>
            <button class="btn btn-danger btn-xs delete-reply-button" id="<?php echo $reply->replyID; ?>">Delete</button>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
</ul>

</li>

<?php endforeach; ?>
<?php endif; ?>