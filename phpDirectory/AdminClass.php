<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'db_connect.php';
class AdminClass{
    private $connection;
    function __construct() {
     $this->connection = mysqli_connect(host, user, password, db);
    }
    public function _register($param){
        if(mysqli_num_rows(mysqli_query($this->connection, "select * from subscribers where userName = '$param[1]'")) != 0){
            return array(0,"Please choose another user name.");
        }
        elseif(mysqli_num_rows(mysqli_query($this->connection, "select * from subscribers where email_of_user = '$param[2]'")) != 0){
            return array(0,"Please choose another email.");
        }
        elseif($param[3] != $param[4]){
            return array(0,"Password and repeat password must be same.");
        }else{
            mysqli_query($this->connection, "insert into subscribers "
                    . "(name_of_user,userName,email_of_user,password_of_user,profile_img)"
                    . " values ('$param[0]','$param[1]','$param[2]','$param[3]','images/photo.jpg')");
            return array(1,"User registered successfully.");
        }
    }
    public function _login($param){
        if(mysqli_num_rows(mysqli_query($this->connection, "select * from subscribers where userName = '$param[0]'")) == 0){
            return array(0,"There is no such user, please register yourself.");
        }
        elseif(mysqli_num_rows(mysqli_query($this->connection, "select * from subscribers where userName = '$param[0]' and password_of_user = '$param[1]'")) == 0){
            return array(0,"Error in login.");
        }else{
            $result = mysqli_fetch_assoc(mysqli_query($this->connection, "select * from subscribers where userName = '$param[0]'"));
            
            if(!isset($_SESSION))session_start ();
            $_SESSION['user_id'] = $result['userId'];
            $_SESSION['user_name'] = $result['name_of_user'];
            return array(1,"User logged in.");
        }
    }
    public function _mailPass($param){
        if(mysqli_num_rows(mysqli_query($this->connection, "select * from subscribers where email_of_user = '$param[0]'")) == 0){
            return array(0,"This email is not registered.");
        }else{
           $result = mysqli_fetch_assoc(mysqli_query($this->connection, "select * from subscribers where email_of_user = '$param[0]'")); 
           $mailBody = 'Your password is : '.$result['password_of_user'];
           mail($param[0], "Password", $mailBody);
           return array(1,"Please check your email for password.");
        }
    }

}

