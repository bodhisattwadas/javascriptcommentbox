<?php
require_once 'db_connect.php'; 
require_once 'comments.php'; 
if( isset( $_POST['task'] ) && $_POST['task'] == 'comment_reply' ){ 
                $userId = (int)$_POST['Userid']; 
		$reply = addslashes( str_replace( "\n" , "<br>" ,  $_POST['Reply']) );
                $commentID = (int)$_POST['CommentID'];
                $std = new stdClass();
         if ( class_exists( 'Comments' ) && class_exists('Subscribers')){
                $userInfo = Subscribers::getSubscriber($userId);
                if($userId == null){
                    $std->error = true;
                }		
                $commentInfo = Comments::replyNormal($commentID, serialize(array($reply)), $userId);     
                if ( $commentInfo == null ){
                    $std->error = true;		
                }  
                $std->user = $userInfo;
                $std->comment = $commentInfo;
                echo json_encode($std);
	}
	else{
            header('location: /');
        }
}
elseif( isset( $_POST['task'] ) && $_POST['task'] == 'modal_comment_reply' ){
                $userId = (int)$_POST['Userid']; 
		$commentTitle = addslashes( str_replace( "\n" , "<br>" ,  $_POST['modalTitle']) );
                $commentBody = addslashes( str_replace( "\n" , "<br>" ,  $_POST['modalDescription']) );
                $commentImage = addslashes( str_replace( "\n" , "<br>" ,  $_POST['modalImage']) );
                $reply = array('title'=>$commentTitle,'body'=>$commentBody,'image'=>$commentImage);
                $commentID = (int)$_POST['CommentID'];
                $std = new stdClass();
         if ( class_exists( 'Comments' ) && class_exists('Subscribers')){
                $userInfo = Subscribers::getSubscriber($userId);
                if($userId == null){
                    $std->error = true;
                }		
                $commentInfo = Comments::replyModal($commentID, serialize($reply), $userId);     
                if ( $commentInfo == null ){
                    $std->error = true;		
                }  
                $std->user = $userInfo;
                $std->comment = $commentInfo;
                echo json_encode($std);
	}
	else{
            header('location: /');
        }
    //echo json_encode('working well');
}

