<?php
function _getTitle($url){
$doc = new DOMDocument();
@$doc->loadHTML(@file_get_contents($url));
$titlelist = $doc->getElementsByTagName("title");
    if($titlelist->length > 0){
        return $titlelist->item(0)->nodeValue;
    }else{
        return 'error';
    }
}
function _getImage($url){
 try{
    $tags = @get_meta_tags($url);
    return $tags['twitter:image:src'];  
 } catch (Exception $ex) {
    return 'No image';
 }   
 
}
function _getDescription($url){
    try{
         $tags = @get_meta_tags($url);
         return $tags['twitter:description'];
    } catch (Exception $ex) {
         return 'No description';
    }
}
if(isset($_POST['URL'])){
    $URL = $_POST['URL'];
    echo json_encode(array(_getTitle($URL),_getDescription($URL),_getImage($URL)));
}

