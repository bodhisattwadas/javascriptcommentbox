<?php
require_once 'comments.php'; 
require_once 'subscribers.php';
if(!isset($_SESSION))session_start();
if( isset( $_POST['task'] ) && $_POST['task'] == 'comment_insert' ){ 
                $userId = (int)$_POST['Userid']; 
		$comment = addslashes( str_replace( "\n" , "<br>" ,  $_POST['Comment']) );
		
                $std = new stdClass();
                $std->user = null;
                $std->comment = null;
                $std->error = false;
                
                
		
         if ( class_exists( 'Comments' ) && class_exists('Subscribers')){
                $userInfo = Subscribers::getSubscriber($userId);
                if($userId == null){
                    $std->error = true;
                }
		//$commentInfo = Comments::insert(serialize($comment), $userId); 
                if(!empty($_SESSION['fileName'])){
                    $commentInfo = Comments::insertNormal(serialize(array($comment,$_SESSION['fileName'])), $userId);
                }else{
                    $commentInfo = Comments::insertNormal(serialize(array($comment)), $userId);
                }
                
                //session_destroy();
                if ( $commentInfo == null ){
                    $std->error = true;		
                }
                    
                    $std->user = $userInfo;
                    $std->comment = $commentInfo;
		
                echo json_encode($std);
	}
	else{
            header('location: /');
        }

}
elseif( isset( $_POST['task'] ) && $_POST['task'] == 'modal_comment_insert' ){
                $userId = (int)$_POST['Userid']; 
		$commentTitle = addslashes( str_replace( "\n" , "<br>" ,  $_POST['modalHeader']) );
                $commentBody = addslashes( str_replace( "\n" , "<br>" ,  $_POST['modalBody']) );
                $commentImage = addslashes( str_replace( "\n" , "<br>" ,  $_POST['modalImage']) );
                $comment = array('title'=>$commentTitle,'body'=>$commentBody,'image'=>$commentImage);
		
                $std = new stdClass();
                $std->user = null;
                $std->comment = null;
                $std->error = false;
                
                
		
         if ( class_exists( 'Comments' ) && class_exists('Subscribers')){
                $userInfo = Subscribers::getSubscriber($userId);
                if($userId == null){
                    $std->error = true;
                }
		//$commentInfo = Comments::insert(serialize($comment), $userId); 		
                $commentInfo = Comments::insertModal(serialize($comment), $userId);     
                if ( $commentInfo == null ){
                    $std->error = true;		
                }
                    
                    $std->user = $userInfo;
                    $std->comment = $commentInfo;
		
                echo json_encode($std);
	}
	else{
            header('location: /');
        }

}
