<?php
require_once 'db_connect.php'; 
require_once 'subscribers.php';
class Comments {
public static function getComments( ){
	$connection = mysqli_connect(host,user,password,db);
        $output = array();
        $query = mysqli_query($connection, "SELECT * FROM comments order by comment_id DESC");	
        if($query){
          if(mysqli_num_rows($query)>0)  {
              while($row = mysqli_fetch_object($query)){
                  $output[] = $row;
              }
          }
        }
        return $output;
}
public static function getReplies($commID){
	$connection = mysqli_connect(host,user,password,db);
        $output = array();
        $query = mysqli_query($connection, "SELECT * FROM reply_table where commentID = '$commID' order by replyID DESC");	
        if($query){
          if(mysqli_num_rows($query)>0)  {
              while($row = mysqli_fetch_object($query)){
                  $output[] = $row;
              }
          }
        }
        return $output;
}	
	
public static function insertNormal($comment_text, $userId){ 
        $connection = mysqli_connect(host,user,password,db);
        $query = mysqli_query($connection, "insert into comments values ('','$comment_text','$userId')");
        if($query){
            $insert_id = mysqli_insert_id($connection);
            $std = new stdClass();
            $std->comment_id = $insert_id;
            $std->comment = unserialize($comment_text);
            $std->userId = (int)$userId;
        } 
        return $std;
}
public static function insertModal($comment_text, $userId){ 
        $connection = mysqli_connect(host,user,password,db);
        $query = mysqli_query($connection, "insert into comments values ('','$comment_text','$userId')");
        if($query){
            $insert_id = mysqli_insert_id($connection);
            $std = new stdClass();
            $comment = unserialize($comment_text);
            $std->comment_id = $insert_id;
            $std->commentTitle = $comment['title'];
            $std->commentDescription = $comment['body'];
            $std->commentImage = $comment['image'];
            $std->userId = (int)$userId;
        } 
        return $std;
} 
 
 public static function update( $data ){
		
		
}
 
 public static function delete( $commentId ){
	$connection = mysqli_connect(host,user,password,db);	
        $query = mysqli_query($connection, "delete from comments where comment_id = '$commentId'");
        if($query){
            mysqli_query($connection, "delete from reply_table where commentID = '$commentId'");
            return true;
        }
        return null;
		
}
public static function replyNormal($commentID,$replyText,$userId){
    $connection = mysqli_connect(host,user,password,db);
    $query = mysqli_query($connection, "insert into reply_table values ('','$commentID','$userId','$replyText')");
        if($query){
            $insert_id = mysqli_insert_id($connection);
            $std = new stdClass();
            $std->id = $insert_id;
            $std->user = $userId;
            $std->reply = unserialize($replyText);
            $std->comment = $commentID;
            $std->error = false;
        } 
        return $std;
}
public static function replyModal($commentID,$replyText,$userId){
    $connection = mysqli_connect(host,user,password,db);
    $query = mysqli_query($connection, "insert into reply_table values ('','$commentID','$userId','$replyText')");
        if($query){
            $insert_id = mysqli_insert_id($connection);
            $std = new stdClass();
            $comment = unserialize($replyText);
            $std->id = $insert_id;
            $std->user = $userId;
            $std->replyTitle = $comment['title'];
            $std->replyDescription = $comment['body'];
            $std->replyImage = $comment['image'];
            $std->comment = $commentID;
            $std->error = false;
        } 
        return $std;
}
public static function replyDelete( $reply ){
	$connection = mysqli_connect(host,user,password,db);	
        $query = mysqli_query($connection, "delete from reply_table where replyID = '$reply'");
        if($query){
            return true;
        }
        return null;
		
}
}
