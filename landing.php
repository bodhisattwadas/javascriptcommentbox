<?php 
    if(!isset($_SESSION)){
        session_start();
        require_once 'phpDirectory/comments.php';
        $userID = $_SESSION['user_id'];
        $userName = $_SESSION['user_name'];
    }
    if($_SESSION['user_id'] == ''){
        session_destroy();
        header('Location: index.php');
    }
    if(isset($_GET['log-out'])){
        session_destroy();
        header('Location: index.php');
    }
    
    if(isset($_GET['file'])){
        $storeFolder = 'uploads';   //2
        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name']; 
            $targetPath = dirname( __FILE__ ) . DIRECTORY_SEPARATOR. $storeFolder . DIRECTORY_SEPARATOR;
            $fileName = $_FILES['file']['name'];
            $toAr = explode(".", $fileName);
            $extension = strtolower($toAr[1]);
            $fileNewName = "file_".((new DateTime())->getTimestamp()).".".$extension; 
            $targetFile =  $targetPath. $fileNewName; 
            move_uploaded_file($tempFile,$targetFile); 
            $_SESSION['fileName'] = $fileNewName;
        }
    }
?>
<!-- sample line -->
<!DOCTYPE html>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<html>
	<head>
		<title>Comment Box</title>
		<link href="css/layout.css" rel="stylesheet"/>
                <link href="css/bootstrap.min.css" rel="stylesheet">
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
                <link href="https://fonts.googleapis.com/css?family=Quicksand:300" rel="stylesheet">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.2.0/dropzone.css" />
		<script type="text/javascript" src="js/jquery.js"></script>
                <script src="js/bootstrap.min.js"></script>
		<script type="text/javascript" src="js/comment_insert.js?t=<?php echo time(); ?>"></script>
                <script type="text/javascript" src="js/comment_delete.js?t=<?php echo time(); ?>"></script>
                <script type="text/javascript" src="js/comment_reply.js?t=<?php echo time(); ?>"></script>
		<script type="text/javascript" src="js/script.js"></script>
                <script src="js/dropzone.js"></script>
                <style>
                    textarea {
                        resize: none;
                    }
                    .dropzone{
                        border-radius: 5px;
                        border : thin dashed;
                    }
                    body{
                        padding: 5px;
                        font-family: 'Quicksand', sans-serif;
                    }
                </style>
	</head>
        <body>
            <div class="row">
               <div class="col-md-2"></div>
               <div class="col-md-8"><h2 style="text-align: center;">Welcome <?php echo strtoupper($userName); ?></h2></div>
               <div class="col-md-2"><a class="btn btn-danger btn-sm" href="landing.php?log-out" role="button">Logout <?php echo $userName; ?></a></div>
            </div>
            <div class="row">
                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <div class="container-fluid" style="border:1px solid #cecece;padding: 5px;">
                                    <div class="form-group" style="text-align: center;">
                                    <div class="row">
                                        <div class="col-md-12"><textarea class="form-control comment-insert-text" rows="5" id="comment-post-text"></textarea></div>       
                                    </div>
                                    <hr> 
                                    <div class="row"><div id="image-holder"></div></div>
                                    </div>
                                    <div class="btn-group pull-right">
                                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#imageModal"><i class="fa fa-camera" aria-hidden="true"></i></button>
                                        <button type="button" class="btn btn-primary" id="comment-post-btn">Post</button>
                                    </div>
                            </div>
                            <div class="container-fluid" style="border:1px solid #cecece;">
					<ul class="comments-holder-ul">
                                            <?php require_once 'phpDirectory/comments.php'; ?>
                                            <?php $comments = Comments::getComments(); ?>
                                            <?php require_once 'phpDirectory/comment_box.php'; ?>
					</ul>
                            </div>
                        <div class="col-md-2"></div>
                    </div>
			
	 </div>
<input type="hidden" id="userID" value="<?php echo $userID; ?>"/>
<input type="hidden" id="userName" value="<?php echo $userName; ?>"/>
                
                
                
                
<div id="commentPostModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h4 class="modal-title" id="modalHeader">xxx</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-2"><img src="images/photo.jpg" alt="Mountain View" style="width:100%;" id="modalImage"></div>
            <div class="col-md-10"><p style="width:95%;display:inline-block;" id="modalBody"></p></div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" id="modal-post-button">Post</button>
    </div>
  </div>
</div>
</div>
<div id="myReplyModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <div id="commentID" style="visibility: hidden">xxx</div>
      <h4 class="modal-title" id="modalHeaderReply">xxx</h4>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-2"><img src="images/photo.jpg" alt="Mountain View" style="width:100%;" id="modalImageReply"></div>
            <div class="col-md-10"><p style="width:95%;display:inline-block;" id="modalBodyReply"></p></div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" onclick="replyModalOnclick()">Reply</button>
    </div>
  </div>
</div>
</div>
<div id="imageModal" class="modal fade" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-body">
        <form action="landing.php?file" class="dropzone" id="my-awesome-dropzone"></form>    
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-success" data-dismiss="modal">Done!!</button>
    </div>
  </div>
</div>
</div>
<script>

</script>            
</body>
</html>